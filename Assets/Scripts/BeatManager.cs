using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatManager : MonoBehaviour
{
    public delegate void OnHitSpace();
    public OnHitSpace onHitSpace;

    public LineRenderer[] paths;

    public List<NoteSetup> notesToBeSpawned = new List<NoteSetup>();

    public GameObject notePrefab;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < 100; ++i)
        {
            notesToBeSpawned.Add(new NoteSetup(paths[Random.Range(0, 4)], Time.time + i, Time.time + i + 1.5f, notePrefab, Time.time));
            StartCoroutine(notesToBeSpawned[i].Spawn());
        }
        foreach(NoteSetup notes in notesToBeSpawned)
        {
            onHitSpace += notes.HitNote;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            onHitSpace.Invoke();
    }

    public void SpawnOnBeat()
    {
        //Debug.Log("Beat");
    }

    //This event will be called every frame while music is playing
    public void OnSpectrum(float[] spectrum)
    {
        //The spectrum is logarithmically averaged
        //to 12 bands

        for (int i = 0; i < spectrum.Length; ++i)
        {
            Vector3 start = new Vector3(i, 0, 0);
            Vector3 end = new Vector3(i, spectrum[i], 0);
            Debug.DrawLine(start, end);
        }
    }
}
