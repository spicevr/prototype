using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Note : MonoBehaviour
{
    
    [System.NonSerialized]
    public bool canBeHit = false;
    public Vector3 moveTo;
    public float hitTime;
    public float spawnTime;
    public Vector3 startPos;
    private float lerpAlpha = 0;
    Rigidbody rb;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("HitBox"))
            canBeHit = true;

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("HitBox"))
        {
            Destroy(gameObject);
        }
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        float distance = Vector3.Distance(transform.position, moveTo);
        float speed = distance / (hitTime - spawnTime);
        //rb.velocity = (moveTo - startPos).normalized * speed;
        rb.AddForce((moveTo - startPos).normalized * speed, ForceMode.VelocityChange);
        Debug.Log(rb.velocity);
    }

    public void Hit()
    {
        if(canBeHit)
        {
            Destroy(gameObject);
            Debug.Log("HIT");
        }
    }
}
