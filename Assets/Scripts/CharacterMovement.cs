using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    delegate void OnHitRight();
    delegate void OnHitLeft();
    private OnHitRight onHitRight;
    private OnHitLeft onHitLeft;
    public Transform[] characterLocations;
    private int currentLocation = 0;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = characterLocations[currentLocation].position;
        onHitRight += MoveRight;
        onHitLeft += MoveLeft;
        onHitLeft += UpdatePosition;
        onHitRight += UpdatePosition;
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
    }

    void MoveRight()
    {
        if (currentLocation >= characterLocations.Length - 1)
            return;
        ++currentLocation;
    }

    void MoveLeft()
    {
        if (currentLocation <= 0)
            return;
        --currentLocation;
    }

    void UpdatePosition()
    {
        transform.position = characterLocations[currentLocation].position;
    }

    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            onHitLeft.Invoke();
        if (Input.GetKeyDown(KeyCode.RightArrow))
            onHitRight.Invoke();
    }
}
