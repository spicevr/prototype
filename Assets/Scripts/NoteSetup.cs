using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteSetup
{
    public Note note;
    public float spawnTime;
    public float hitTime;
    private LineRenderer lineRenderer;
    private GameObject prefab;
    private float timeNow;
    public NoteSetup(LineRenderer line, float spawnTimeInput, float hitTimeInput, GameObject notePrefab, float timeNowInput)
    {
        spawnTime = spawnTimeInput;
        hitTime = hitTimeInput;
        lineRenderer = line;
        prefab = notePrefab;
        timeNow = timeNowInput;
    }

    public IEnumerator Spawn()
    {
        while(Time.time < timeNow + spawnTime)
        {
            yield return new WaitForEndOfFrame();
        }
        note = GameObject.Instantiate(prefab, lineRenderer.GetPosition(0), Quaternion.identity).GetComponent<Note>();
        note.moveTo = lineRenderer.GetPosition(1);
        note.hitTime = hitTime;
        note.spawnTime = spawnTime;
        note.startPos = lineRenderer.GetPosition(0);
    }

    public void HitNote()
    {
        if(note != null)
        {
            note.Hit();
        }
    }
}
